// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "OnlineMultiplayerGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class ONLINEMULTIPLAYER_API AOnlineMultiplayerGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
