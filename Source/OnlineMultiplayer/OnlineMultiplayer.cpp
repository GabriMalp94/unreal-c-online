// Copyright Epic Games, Inc. All Rights Reserved.

#include "OnlineMultiplayer.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, OnlineMultiplayer, "OnlineMultiplayer" );
